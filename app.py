# coding=utf-8
import base64
import os

from flask import Flask, render_template, request, redirect

app = Flask(__name__)
readme = 'README.md'


def git_syn():
    os.system('git pull')
    os.system('git add -A')
    os.system("git commit -m 'syn' ")
    os.system('git push')
    return 0


@app.route('/', methods=['GET', 'POST'])
def index():
    with open(readme) as readme_f:
        readme_cont = readme_f.read()
        raw_cont = base64.b64decode(readme_cont)
        cont = raw_cont.decode("utf-8")

    if request.method == 'POST':
        type_value = request.form['type']
        vmess_text = request.form['vmess']
        if type_value == 'append':
            cont += '\n' + vmess_text
        else:
            cont = vmess_text
        with open(readme, 'wb') as readme_f:
            encode_cont = base64.b64encode(cont.encode())
            readme_f.write(encode_cont)
        git_syn()
        return redirect('/')

    return render_template('index.html', info=cont)


@app.route('/api', methods=['GET', 'POST'])
def api():
    if request.method == "POST":
        username = request.form['username']
        total_vmess_uris = request.form['total_vmess_uris']

        with open(username, 'wb') as uri_files:
            encode_cont = base64.b64encode(total_vmess_uris.encode())
            uri_files.write(encode_cont)
        git_syn()
        return 'https://gitlab.com/none_null/null/raw/master/%s' % username
    else:
        return 'how are you?'


@app.route('/del_api/<username>', methods=['GET', 'POST'])
def del_api(username):
    if os.path.isfile(username):
        os.remove(username)
    git_syn()
    return 'how are you?'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1608, debug=True)
